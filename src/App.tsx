import * as React from 'react';
import { Layout } from './components/Layout';

export const App: React.FC = _props => (
    <div className="ui container">
        <Layout />
    </div>
);
